package HomeWork1;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

    class Homework1 {
        public static int[] Numbers_array;

        public static void main(String[] args) {

            // Generating a random number

            Random random = new Random();
            int max = 100;
            int min = 0;

            int rand_number = random.nextInt(max - min) + min;
            //  System.out.println(rand_number);


            //getting user's name, and ask for entering number

            System.out.print("What is your name? --> ");
            Scanner sc = new Scanner(System.in);
            String name = sc.nextLine();
            System.out.println("Let the game begin!");
            System.out.print("Please enter a number: ");

            int count = 0;
            int[] Numbers_array = new int[100];
            int number;

            //Game part
            do {

                number = sc.nextInt();
                if (number == (int) number) {
                    Numbers_array[count] = number;

                    count++;
                    if (number < rand_number) {
                        System.out.println("Your number is too small. Please, try again.");

                    } else if (number > rand_number) {
                        System.out.println("Your number is too big. Please, try again.");
                    }
                }
            }

            while (number != rand_number);

            System.out.println("Congratulations, " + name);
            System.out.println("Your numbers: ");
            Arrays.sort(Numbers_array);

            for (int i = 100 - count; i <= 99; i++) {
                System.out.print(Numbers_array[i] + " ");
            }
        }
    }