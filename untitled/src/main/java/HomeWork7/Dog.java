package HomeWork7;

public class Dog extends Pet{
    Species species;

    public Dog(){};

    public Dog(Species species){
        this.species = species;
    }

    @Override
    public void respond(String nickname) {
        System.out.printf("My name is %s and I can play all day long",nickname);
    }

    @Override
    public String toString() {
        return "Dog{" + "species =" + species + "}";
    }
}