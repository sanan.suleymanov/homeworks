package HomeWork7;


public class RoboCat extends Pet{
    Species species;

    public RoboCat(Species species){
        this.species = species;
    }

    @Override
    public void respond(String nickname) {
        System.out.printf("My name is %s and I have 100 lives instead of 9");
    }
}
