package HomeWork7;


public enum Species {
    DOG(false, 4, true),
    UNICORN(true, 4, true),
    CAT(false,4,true),
    FiSH(false,0,false),
    DOMESTICCAT(false,4,true),
    ROBOCAT(true, 6, false),
    UNKNOWN;


    @Override
    public String toString() {
        return "Pet{canfly=" + canfly +
                ", numberOflegs=" + numberOflegs +
                ", hasFur=" + hasFur +
                "}";
    }

    private  boolean canfly;
    private int numberOflegs;
    private boolean hasFur;

    Species() {

    }

    Species(boolean canfly, int numberOflegs, boolean hasFur)
    {
        this.canfly=canfly;
        this.numberOflegs=numberOflegs;
        this.hasFur=hasFur;

    }
}