package HomeWork7;


public class Fish extends Pet{
    Species species;

    public Fish(Species species) {
        this.species = species;
    }

    @Override
    public void respond(String nickname) {
        System.out.printf("My name is %s I can swim without drowning", nickname);
    }

    @Override
    public String toString() {
        return "Fish{" + "species =" + species + "}";
    }
}
