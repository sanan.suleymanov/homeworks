package HomeWork7;

import java.util.Arrays;

public abstract class Pet {

    private Species species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits = new String[3];

    //CONSTRUCTORS
    public Pet() {

    }

    public Pet(String nickname) {
        this.nickname = nickname;
    }

    public Pet(String nickname, int age, int trickLevel, String[] habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }


    //METHODS
    public void eat(){
        System.out.println("I am eating");
    }

    public abstract void respond(String nickname);

    public void foul(){
        System.out.println("I need to cover it up");
    }

    @Override
    public String toString() {
        return String.format(this.species+ "{" + "nickname" + "=" +
                this.nickname + ", " + "age" + "=" + this.age + ", " + "trickLevel" + "=" +
                this.trickLevel + ", " + "habits" + "=" + Arrays.toString(this.habits) + ", " + "characteristics [ " +
                species.toString() + " ] " + "}");
    }

    public String getNickname() {
        return nickname;
    }

    public Species getSpecies() {
        return species;
    }

    public int getAge() {
        return age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

}