package HomeWork5;

public class Main {
    public static void main(String[] args) {
        String[] habbits = {"playing", "walking", "eating"};
        String[][] schedule = {{"Sunday", "Meditation"},
                {"Monday", "Family Time"},
                {"Tuesday", "Gym"},
                {"Wednesday", "Shopping"},
                {"Thursday","Walkig"},
                {"Friday", "Playing football"},
                {"Saturday", "Hanging out with friends"}
        };

        Human father = new Human("Leonardo", "DiCaprio", 1974, 85, schedule);
        Human mother = new Human("Camila", "Morrone", 1977, 78, schedule);
        Pet pet = new Pet("Cat", "Tom", 4, 90, habbits);
        Human child = new Human("Michael", "Karleone", 1997, 85, schedule);
        Family family = new Family(mother, father,pet, 1);

        System.out.println("Number of family is " + family.countFamily());

        family.addChild(child);


        System.out.println("Child added");

        System.out.println("Number of family is " + family.countFamily());

        System.out.println(family.toString());
        System.out.println();


        System.out.println(family.deleteChild(1) ? "Child deleted" : "There is no child to delete");

        System.out.println("Number of family is " + family.countFamily());

        System.out.println(family.deleteChild(1) ? "Child deleted" : "There is no child to delete");


        System.out.println("Testing Equals and Hascode methodes: ");

        System.out.println("child = father --> " + child.equals(father));
        System.out.println("child = child --> " + child.equals(child));


        System.out.println("HashCode of child --> " + child.hashCode());

    }
}
